<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Panel Routes
|--------------------------------------------------------------------------
||
*/


Route::get('/', 'PanelController@index')->name('panel');

// Rutas de productos
Route::resource('products', 'ProductController');



