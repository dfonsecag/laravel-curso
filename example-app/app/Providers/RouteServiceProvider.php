<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/';


    protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();
    }

    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();

        // Creamos esta para administrar a los administradores de los usuarios
        $this->mapPanelRoutes();
    }

    protected function mapApiRoutes()
    {         
            Route::middleware('api')
                ->namespace($this->namespace)
                ->prefix('api')
                ->group(base_path('routes/api.php'));
    }

    protected function mapWebRoutes()
    {
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
    }

     /**
     * Define el admin panel routes de el administrador
     *
     * @return void
     */

    protected function mapPanelRoutes()
    {
            Route::prefix('panel')
            // hay que agregar en el kernel el midleware is.admin
                ->middleware(['web','auth','is.admin'])
                ->namespace("{$this->namespace}\Panel")
                ->group(base_path('routes/panel.php'));
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }
}
