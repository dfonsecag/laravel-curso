<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use App\Services\CartService;
use App\Order;

class OrderController extends Controller
{
    public $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
        $this->middleware('auth');
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cart = $this->cartService->getFromCookie();

        if(!isset($cart) || $cart->products->isEmpty()){
            return redirect()
                ->back()
                ->withErrors("Su carrito esta vacío");
        }

        return view('orders.create')->with([
            'cart' => $cart
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use($request) {
            $user = $request->user();
        
            $order = $user->orders()->create([
                'status' => 'pendiente',
            ]);
        
            $cart = $this->cartService->getFromCookie();
        
            $cartProductsWithQuantity = $cart
                ->products
                ->mapWithKeys(function ($product){
                
                $quantity = $product->pivot->quantity;

                if($product->stock < $quantity){
                    throw ValidationException::withMessages([
                        'product' => "No hay suficiente stock para el producto de {$product->title}"
                    ]);
                }
        
                // Reducir la cantidad del producto del inventario
                $product->decrement('stock', $quantity);

                $element[$product->id] = ['quantity' => $quantity];

                return $element;
            });
            // dd($cartProductsWithQuantity);
            $order->products()->attach($cartProductsWithQuantity->toArray());

            return redirect()->route('orders.payments.create', ['order' => $order]);
        }, 5);        

    }

    
}
