<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\PanelProduct;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;
use App\Scopes\AvailableScope;


class ProductController extends Controller
{

    public function index()
    {
        return view('products.index')->with([
            'products' =>  PanelProduct::without('images')->get(),
        ]);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(ProductRequest $request)
    {
        // dd(request()->all(), $request->all(), $request->validated());

        $product = PanelProduct::create($request->validated());
        return redirect()->route('products.index')
         ->withSuccess("El producto con el título  $product->title fue creado exitosamente");
        //   ->with(['success' => "El producto con el título { $product-> title} fue creado exitosamente"]);
    }

    public function show(PanelProduct $product)
    { 
        // $product = Product::findOrFail($product); se reemplaza por el parametro Product $product

        return view('products.show')->with([
            'product' => $product,
            'html' => '<h1>Hoola</h1>'
        ]);
    }
    
    public function edit(PanelProduct $product)
    {
        return view('products.edit')->with([
            'product' => $product
        ]);
    }
    
    public function update( ProductRequest $request,PanelProduct $product)
    {        
        $product->update($request->validated());

        return redirect()
         ->route('products.index')
         ->withSuccess("El producto con el título  $product->title fue actualizado exitosamente");
    }

    public function destroy(PanelProduct $product)
    {

        $product->delete();

        return redirect()->route('products.index')
        ->withSuccess("El producto con el título  $product->title fue eliminado exitosamente");;;
    }
}
