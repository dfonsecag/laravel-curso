<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\AvailableScope;
use App\Cart;
use App\Order;
use App\Images;

class Product extends Model
{
    use HasFactory;
    
    protected $table = 'products';

    // Para cargar las diferentes relaciones para ese modelo
    protected $with = ['images'];
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'price',
        'stock',
        'status'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new AvailableScope);
    }

    public function carts()
    {
        return $this->morphedByMany(Cart::class,'productable')->withPivot('quantity');
    }

    public function orders()
    {
        return $this->morphedByMany(Order::class,'productable')->withPivot('quantity');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    
    public function scopeAvailable($query)
    {
        $query->where('status', 'disponible');
    }

    public function getTotalAttribute()
    {
        return $this->pivot->quantity * $this->price;
    }

}
