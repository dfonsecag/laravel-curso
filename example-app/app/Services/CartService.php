<?php

namespace App\Services;
use App\Cart;
use Illuminate\Support\Facades\Cookie;

class CartService
{

    // Atributo de la cookie
    protected $cookieName;
    // Atributo de expiracion de la cookie
    protected $cookieExpiration;
    
    public function __construct()
    {
        $this->cookieName = config('cart.cookie.name');
        $this->cookieExpiration = config('cart.cookie.expiration');
    }

    public function getFromCookie()
    {
         // Obtener datos de la cookie
         $cartId = Cookie::get($this->cookieName);

         $cart = Cart::find($cartId);

         return $cart;
    }

    public function getFromCookiesOrCreate()
    {
        $cart = $this->getFromCookie();

        return $cart ?? Cart::create();
    }

    public function makeCookie(Cart $cart)
    {
        return Cookie::make($this->cookieName, $cart->id, $this->cookieExpiration);
    }

    public function countProducts()
    {
        $cart = $this->getFromCookie();
        if($cart != null) {
            return $cart->products->pluck('pivot.quantity')->sum();
        }

        return 0;
    }

}