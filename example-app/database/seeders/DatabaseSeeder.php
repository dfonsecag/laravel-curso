<?php

namespace Database\Seeders;

use App\Product;
use App\User;
use App\Order;
use App\Payment;
use App\Image;
use App\Cart;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

      
    //    Crear usuario y sus imagenes
       $users = User::factory(20)
       ->create()
       ->each(function ($user){
           $image = Image::factory()
            ->user()
            ->make();

            $user->image()->save($image);
       });


    //  Genera ordenes y pagos en semillas  
       $orders = Order::factory(10)
       ->make()
       ->each(function($order) use ($users) {
            //
            $order->customer_id = $users->random()->id;
            $order->save();

            $payment = Payment::factory()->make();

            // $payment->order_id = $order->id;
            // $payment->save();
            // O se hace esto para simplificar
            $order->payment()->save($payment);

       });

    //    Crear carritos
        $carts = Cart::factory(20)->create();

        $products = Product::factory(50)
        ->create()
        ->each(function ($product) use ($orders, $carts){
            
            // Agregar productos a una orden
            $order = $orders->random();
            $order->products()->attach([
                $product->id => ['quantity' => mt_rand(1,3)]
            ]);
            // Agregar productos a un carrito
            $cart = $carts->random();
            $cart->products()->attach([
                $product->id => ['quantity' => mt_rand(1,3)]
            ]);

            $images = Image::factory(mt_rand(2,4))->make();

            $product->images()->saveMany($images);

        });
      
    }
}
