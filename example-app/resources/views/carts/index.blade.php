@extends('layouts.app')

@section('content')
    <h1>Su carrito</h1>
    @if(!isset($cart) ||  $cart->products->isEmpty())
        <div class="alert alert-warning">
           Su carrito esta vacío
        </div>
        
           
    @else
        <h4 class="text-center"><strong>El Total del carrito: ${{ $cart->total }}</strong></h4>
        <a class="btn btn-success mb-3" href="{{ route('orders.create') }}">Empezar Orden</a>
        <div class="row">
            @foreach ($cart->products as $product)
            <div class="col-3">
              @include('components.product-card')
            </div>
            @endforeach           
        </div>
    @endempty

@endsection
 