@extends('layouts.app')

@section('content')
    <h1>Editar producto</h1>

    <form method="POST" action="{{ route('products.update',['product'=> $product->id]) }}" >
        @csrf
        @method('PUT')
        <div class="fomr-row">
            <label for="title">Título</label>
            <input class="form-control" type="text" name="title" value="{{ old('title') ?? $product->title }}" required >
        </div>

        <div class="fomr-row">
            <label for="description">Descripción</label>
            <input class="form-control" type="text" name="description" value="{{ old('description') ?? $product->description }}"  required>
        </div>

        <div class="fomr-row">
            <label for="price">Precio</label>
            <input class="form-control" type="number" name="price" value="{{ old('price') ?? $product->price }}" min="1.00" step="0.01"  required>
        </div>

        <div class="fomr-row">
            <label for="stock">Stock</label>
            <input class="form-control" type="number" name="stock" value="{{ old('stock') ?? $product->stock }}" min="0"  required>
        </div>

        <div class="fomr-row">
            <label for="status">Estado</label>
            <select name="status" class="custom-select"  required>                
                <option {{ old('stock') == 'disponible' ? 'selected' : ($product->status == 'disponible' ? 'selected' : '')}} value="disponible" >Disponible</option>
                <option {{ old('stock') == 'no disponible' ? 'selected' : ($product->status == 'no disponible' ? 'selected' : '')}} value="no disponible" >No disponible</option>
                {{-- <option  value="no disponible" >No disponible</option> --}}
            </select>
        </div>
        <div class="fomr-row mt-3">
           <button type="submit" class="btn btn-primary btn-lg ">Editar Producto</button>
        </div>

    </form>
@endsection
 