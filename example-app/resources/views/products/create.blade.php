@extends('layouts.app')

@section('content')
    <h1>Crear producto</h1>

    <form method="POST" action="{{ route('products.store') }}">
        @csrf
        <div class="fomr-row">
            <label for="title">Título</label>
            <input class="form-control" type="text" name="title" value="{{ old('title') }}" required>
        </div>

        <div class="fomr-row">
            <label for="description">Descripción</label>
            <input class="form-control" type="text" name="description" value="{{ old('description') }}" required>
        </div>

        <div class="fomr-row">
            <label for="price">Precio</label>
            <input class="form-control" type="number" name="price" min="1.00" step="0.01" value="{{ old('price') }}" required>
        </div>

        <div class="fomr-row">
            <label for="stock">Stock</label>
            <input class="form-control" type="number" name="stock" min="0" value="{{ old('stock') }}" required>
        </div>

        <div class="fomr-row">
            <label for="status">Estado</label>
            <select name="status" class="custom-select"  required>
                <option value="" selected>Seleccione el estado</option>
                <option value="disponible" {{ old('status') == 'disponible' ? 'selected' : '' }}>Disponible</option>
                <option value="no disponible" {{ old('status') == 'no disponible' ? 'selected' : '' }}>No disponible</option>
            </select>
        </div>
        <div class="fomr-row mt-3">
           <button type="submit" class="btn btn-primary btn-lg">Crear Producto</button>
        </div>

    </form>
@endsection
 