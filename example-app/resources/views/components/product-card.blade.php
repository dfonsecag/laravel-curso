<div class="card">
    <div id="carousel{{ $product->id }}" class="carousel slide" data-bs-ride="carousel">
        
        <div class="carousel-inner">
            @foreach ($product->images as $image)
            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <img src="{{ asset($image->path) }}" class="d-block with-100 card-img-top" height="500">
            </div>
            @endforeach
       
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carousel{{ $product->id }}" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carousel{{ $product->id }}" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    <div class="card-body">
        <h4 class="text-right"> <strong>${{ $product->price }}</strong> </h4>
        <h5 class="card-title">{{ $product->title }}</h5>
        <p class="card-text">{{ $product->description }}</p>
        <p class="card-text"><strong>{{ $product->stock }} disponible{{$product->stock > 1 ? 's' :''}}</strong></p>
        {{-- Eliminar del carrito  --}}
        @if (isset($cart))
            <p class="card-text"><strong>{{ $product->pivot->quantity }} en su carrito ( $ {{ $product->total }} )</strong></p>
            <form 
            class="d-inline" 
            method="POST"
            action="{{ route('products.carts.destroy',['cart'=>$cart->id,'product'=>$product->id]) }}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Eliminar del Carrito</button>
            </form>
       {{-- Agregar al carrito  --}}
        @else
            <form 
            class="d-inline" 
            method="POST"
            action="{{ route('products.carts.store',['product'=>$product->id]) }}">
            @csrf
            <button type="submit" class="btn btn-success">Agregar al Carrito</button>
            </form>
        @endif
    </div>
 
</div>
